class CompaniesController < ApplicationController
  def index
    @companies = Company.all
  end

  def show
  end

  private

  def company_params
    params.require(:company).permit(:name)
  end
end
