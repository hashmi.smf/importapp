ActiveRecord::Schema.define(version: 20160724074551) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories_operations", force: :cascade do |t|
    t.integer "category_id"
    t.integer "operation_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "operations", force: :cascade do |t|
    t.string   "invoice_num",                             null: false
    t.date     "invoice_date",                            null: false
    t.date     "operation_date",                          null: false
    t.decimal  "amount",         precision: 10, scale: 2, null: false
    t.string   "reporter"
    t.text     "notes"
    t.string   "status",                                  null: false
    t.string   "kind",                                    null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "company_id"
  end

  add_index "operations", ["company_id"], name: "index_operations_on_company_id", using: :btree

end
